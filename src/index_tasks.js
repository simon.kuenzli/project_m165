function renderTasks(tasks) {
    const tbody = document.querySelector("#tasks tbody");
    tasks.forEach((task) => {
        const row = document.createElement('tr');

        const idCell = document.createElement('td');
        idCell.innerText = task.id;
        const titleCell = document.createElement('td');
        titleCell.innerText = task.title;
        const completedCell = document.createElement('td');
        completedCell.innerText = task.completed;

        row.appendChild(idCell);
        row.appendChild(titleCell);
        row.appendChild(completedCell);
        tbody.appendChild(row);

    });
}

function IndexTask() {
    fetch('http://localhost:3000/tasks', {
        method: 'GET',
        credentials: "include",
        headers: {
        'Content-Type': 'application/json'
        }
    }).then((response) => {
        return response.json();
    }).then((data) => {
        renderTasks(data);
    }).catch(() => {
        alert("Something went wrong :/");
    });
}

function ShowCurrentUser() {
    fetch('http://localhost:3000/status', {
        method: "GET",
        credentials: "include",
        headers: {
            'Content-Type': 'application/json'
        }
    })
}



document.addEventListener("DOMContentLoaded", () => {
    IndexTask.call();
})
