function createTask() {
   const task123 = document.getElementById('titleInput');
   const task = {
       "title": task123.value
   }
   fetch('http://localhost:3000/tasks', {
        method: 'POST',
        headers: {
        'Content-Type': 'application/json'
        },
        body: JSON.stringify(task)
    }).then(() => {
        window.location.href = "./index_tasks.html";
    }).catch(() => {
        alert("Something went wrong :/");
    });
};

document.addEventListener("DOMContentLoaded", () => {
    
    const createTaskForm = document.getElementById('createTaskForm');
    createTaskForm.addEventListener('submit', (event) => {
        event.preventDefault();
        const createTaskFormData = new FormData(createTaskForm);
        createTask({title: createTaskFormData.get('title')});
    });
});